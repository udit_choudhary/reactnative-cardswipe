import React, { Component} from 'react';
import {View, Animated} from 'react-native';


class Ball extends Component{

  componentWillMount(){
    // starting position of the object
    this.position = new Animated.ValueXY(0,0);

    // Setting Animation style and starting the animation towards the given xy
    Animated.spring(
      this.position,
      {
        toValue: {x:250, y:600}
      }
    ).start();
  }

  render(){
    return(
      <Animated.View style={this.position.getLayout()}>
        <View style={styles.ball} />
        <View style={styles.ball1} />
        <View style={styles.ball2} />
      </Animated.View>

    );
  }

}


const styles = {
  ball: {
    height: 60,
    width: 60,
    borderRadius: 30,
    borderWidth: 3,
    borderColor: 'black'
  },
  ball1: {
    height: 30,
    width: 30,
    borderRadius: 30,
    borderWidth: 2,
    borderColor: 'red'
  },
  ball2: {
    height: 20,
    width: 20,
    borderRadius: 30,
    borderWidth: 1,
    borderColor: 'green'
  }
}


export default Ball;
